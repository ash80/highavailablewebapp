#!/bin/bash

echo "This script takes parameters that will update AWS Cloudformation Stack"
echo "Please pass the following paramters
        1) Stack name 
        2) File name of the template
        3) Parameters file"

aws cloudformation update-stack \
--stack-name $1 \
--template-body file://$2 \
--parameters file://$3 \
--capabilities $4 \
