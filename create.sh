#!/bin/bash

echo "This script takes parameters that will create AWS Cloudformation Stack"
echo "Please pass the following paramters
        1) Stack name 
        2) File name of the template
        3) Parameters file"

aws cloudformation create-stack \
--stack-name $1 \
--template-body file://$2 \
--parameters file://$3 \
--region=ap-south-1

